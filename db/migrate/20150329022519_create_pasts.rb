class CreatePasts < ActiveRecord::Migration
  def change
    create_table :pasts do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
