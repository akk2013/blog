json.array!(@pasts) do |past|
  json.extract! past, :id, :title, :body
  json.url past_url(past, format: :json)
end
